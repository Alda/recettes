---
title: Tarte à la confiture de framboise et graines de chia
ingredients:
    - name: Confiture de framboise
      quantity: 200
      unit: g
    - name: Graines de chia
      quantity: 2
      unit: c. à s.
    - name: Eau
      quantity: 6
      unit: c. à s.
    - name: Flocons d'avoine
      quantity: 85
      unit: g
    - name: Poudre d'amandes
      quantity: 60
      unit: g
    - name: Sucre
      quantity: 45
      unit: g
    - name: Sel
      quantity: 1
      unit: c. à c.
    - name: Beurre d'amandes
      quantity: 2
      unit: c. à s.
    - name: Huile de coco
      quantity: 3
      unit: c. à s.
    - name: Levure chimique
      quantity: 1
      unit: c. à c.
---

- Mettre les graines de chia et l'eau dans un verre. Mélanger. Laisser reposer 10min.
- Passer les flocons d'avoine au blender pour les réduire en poudre.
- Mixer la poudre d'avoine avec la poudre d'amande, le sucre, le sel et la levure.
- Faire fondre l'huile de coco.
- Ajouter le beurre d'amande, l'huile de coco et les graines de chia au mélange de poudre.
- Mélanger uniformément.
- Répartir les 3/4 de la pâte au fond d'un plat.
- Passer au four pendant 10min à 180°C.
- Recouvrir avec la confiture de framboise.
- Émietter le reste de la pâte.
- Mettre au four pendant 20min à 180°C.
- Servir tiède (ou froid).

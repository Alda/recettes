---
title: Parmentier de patates douces aux lentilles
ingredients:
    - name: Lentilles vertes
      quantity: 200
      unit: g
    - name: Patates douces
      quantity: 1.5
      unit: kg
    - name: Huile d'olive
      quantity: 2
      unit: c. à s.
    - name: Échalottes
      quantity: 2
    - name: Ail
      quantity: 2
      unit: gousse
    - name: Ciboulette
      quantity: 1
      unit: c. à s.
    - name: Persil
      quantity: 2
      unit: c. à s.
    - name: Tamari
      quantity: 2
      unit: c. à s.
    - name: Crème de soja
      quantity: 100
      unit: ml
    - name: Levure maltée
      quantity: 2
      unit: c. à s.
cuisson:
    preparation: 1 heure
    cuisson: 20 minutes
---

- Éplucher et couper en morceaux les patates douces.
- Cuire les patates douces dans une casserolle d'eau bouillante pendant 30 minutes.
- Cuire les lentilles dans une casserolle d'eau bouillante pendant 20 minutes.
- Émincer les échalottes, l'ail et les herbes.
- Faire revenir les échalottes et l'ail dans une poele.
- Ajouter les lentillées égouttées, les herbes et le tamari, laisser cuire pendant 5 à 10 min.
- Égoutter les patates douces et les réduire en purée.
- Répartir les lentilles sur le fond d'un plat à gratin.
- Recouvrir les lentilles avec la purée de patates douces en tassant bien la purée.
- Étaler la crème de soja sur la purée.
- Saupoudrer la levure maltée sur la crème.
- Mettre au four 15 minutes à 180°C.
- Passer au grill 5 minutes pour gratiner.
- Servir.

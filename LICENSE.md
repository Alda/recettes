> N’étant que la liste des ingrédients et la description des opérations nécessaires à la préparation, la recette relève en réalité de la sphère des idées et des informations. Elle n’est donc pas protégeable par le droit d’auteur.

[Les recettes de cuisine sont-elles libres ?](http://romy.tetue.net/les-recettes-de-cuisine-sont-elles-libres)
